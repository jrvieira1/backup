#!/bin/bash

# chmod +x this file
# usage: /path/to/backup.sh target origin1 origin2 origin3 ...
# origin example: user@host:/path/to/folder # with trailing slash to copy contents, without to copy folder

target="$1"
shift  # remove first argument

# validate target directory
if [[ -z "$target" ]]; then
   echo "no target directory"
   exit 1
fi

# create structure if inexistent
mkdir -p $target

# sync and log
for origin in $@; do
   echo "syncing: ${origin} to ${target}"
	rsync -azpvhb -e "ssh -i /home/zero/.ssh/jrvieira_rsa" --backup-dir=.old "$origin" "$target" --delete-after
done

echo "backup done!"
